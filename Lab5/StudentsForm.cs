﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace Lab5
{
    public partial class StudentsForm : Form
    {
        TestDB db = new TestDB(ConfigurationManager.ConnectionStrings["msConn"].ConnectionString);
        public StudentsForm()
        {
            InitializeComponent();
        }

        private void search_stud_Click(object sender, EventArgs e)
        {
            string param = textBox1.Text;
            var custquery = db.GetStudentByName(param);
            List<GetStudentByNameResult> test = new List<GetStudentByNameResult>();
            foreach (GetStudentByNameResult IvanovSProcedure in custquery)
            {
                test.Add(IvanovSProcedure);

            }
            dataGridView1.DataSource = test;
        }

        private void show_subj_Click(object sender, EventArgs e)
        {
            SubjectsForm f = new SubjectsForm();
            f.Show();
        }
    }
}