﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace Lab5
{
    public partial class SubjectsForm : Form
    {
        TestDB db = new TestDB(ConfigurationManager.ConnectionStrings["msConn"].ConnectionString);
        public SubjectsForm()
        {
            InitializeComponent();
        }

        private void search_stud_subj_Click(object sender, EventArgs e)
        {
            string param = textBox1.Text;
            var custquery = db.GetSubjectByName(param);
            List<GetSubjectByNameResult> test = new List<GetSubjectByNameResult>();
            foreach (GetSubjectByNameResult IvanovSProcedure in custquery)
            {
                test.Add(IvanovSProcedure);
            }
            dataGridView1.DataSource = test;
        }
    }
}